<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Ajax extends CI_Controller {
public function __construct() {
		parent::__construct();
    	$this->load->model('countuiesModel');
	}

  public function myData() {
      $countries = $this->countuiesModel->getCountries();

      echo json_encode($countries);
  }
}