<?php
defined('BASEPATH') OR exit('No direct script access allowed');
?>
<!DOCTYPE html>
 <html lang="hu">
  <head>
   <meta charset="UTF-8">
   <meta name="viewport" content="width=device-width, initial-scale=1">
   <title>Feladat :: Elme-Project Kft-től</title>
   <base href="<?php echo base_url(); ?>">
   <link rel="stylesheet" href="<?php echo base_url("/assets/node_modules/bootstrap/dist/css/bootstrap.css"); ?>">
   <script src="https://code.jquery.com/jquery-3.5.1.js"></script>
   <script src="<?php echo base_url("/assets/node_modules/popper.js/dist/popper.min.js"); ?>"></script>
   <script src="<?php echo base_url("/assets/node_modules/bootstrap/dist/js/bootstrap.js"); ?>"></script>
   <link rel="stylesheet" href="<?php echo base_url("/assets/node_modules/bootstrap/dist/css/bootstrap-grid.min.css"); ?>">
   <link rel="stylesheet" href="<?php echo base_url("/assets/node_modules/bootstrap/dist/css/bootstrap-reboot.min.css"); ?>">
   <script src="<?php echo base_url("/assets/node_modules/bootstrap/dist/js/bootstrap.bundle.min.js"); ?>"></script>
   <link rel="stylesheet" href="<?php echo base_url("/assets/css/SajatResponsestyle.css"); ?>">
   <link href="<?php echo base_url("/assets/node_modules/font-awesome/css/font-awesome.min.css"); ?>">
   <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/1.10.23/css/jquery.dataTables.css">
   <script type="text/javascript" charset="utf8" src="https://cdn.datatables.net/1.10.23/js/jquery.dataTables.min.js"></script>
   <meta name="robots" content="noindex, noimageindex, nofollow, nosnippet">
   <meta name="googlebot" content="noindex, noimageindex, nofollow, nosnippet">
    <script type="text/javascript">
    $(document).ready(function(){
      $('#myTable').DataTable({
        "ajax": '<?php echo site_url(); ?>/ajax/myData',
        "columns": [
            { "data": "name" },
            { "data": "euro",
                render: function(data, type) {
                  if(data == 'EUR') {
                    return ''
                  }
                  else {
                    return '<a class="btn btn-primary">'+data+'</>'
                  }
                }
            },
            {
                className: 'f32', // used by world-flags-sprite library
                data: 'alpha2Code',
                render: function(data, type) {
 
                    return '<span class="flag ' + data.toLocaleLowerCase() + '"></span> '
                }
            },
            { "data": "capital" },
            { "data": "population" }
        ]
      });
    });
  </script>
</head>
 <body>