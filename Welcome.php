<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Welcome extends CI_Controller {
	public function __construct() {
		parent::__construct();
    	$this->load->model('countuiesModel');
	}

	public function index()
	{
		$this->countuiesModel->deleteAll();
		$url = "https://restcountries.eu/rest/v2/all?fields=name;alpha2Code;alpha3Code;capital;subregion;population;currencies";

		$json = file_get_contents($url);
		$json = json_decode($json);

		foreach ($json as $key => $value) {
			$cur = "";
			for ($i=0; $i < count($value->currencies); $i++) {
				$code = ""; $name = ""; $symbol = ""; $euro = "";
				if(isset($value->currencies[$i]->code)) {
					$code = $value->currencies[$i]->code;
				}
				if(isset($value->currencies[$i]->name)) {
					$name = $value->currencies[$i]->name;
				}
				if(isset($value->currencies[$i]->symbol)) {
					$symbol = $value->currencies[$i]->symbol;
				}

				if($code != 'EUR') $euro = $code;

				if($code == "" && $name == "" && $symbol != "") $cur .= $symbol;
				if($code == "" && $name != "" && $symbol == "") $cur .= $name;
				if($code != "" && $name == "" && $symbol == "") $cur .= $code;
				if($code == "" && $name != "" && $symbol != "") $cur .= $name.", ".$symbol;
				if($code != "" && $name != "" && $symbol == "") $cur .= $code.", ".$name;
				if($code != "" && $name == "" && $symbol != "") $cur .= $code.", ".$symbol;
				if($code != "" && $name != "" && $symbol != "") $cur .= $code.", ".$name.", ".$symbol;
				if(count($value->currencies) > 1) $cur .= ";";
			}
			$this->countuiesModel->setCountries($json[$key]->name, $json[$key]->alpha2Code, $json[$key]->alpha3Code, $json[$key]->capital, $json[$key]->subregion, $json[$key]->population, $euro, $cur);
		}

		//$euroUrl = "http://www.ecb.europa.eu/stats/exchange/eurofxref/html/index.en.html#dev";
		$letezik = $this->countuiesModel->getCurrency();
		$euroUrl = "https://www.ecb.europa.eu/stats/eurofxref/eurofxref-daily.xml";

		$xml = simplexml_load_file($euroUrl);
		$xmlJson  = json_encode($xml);
		$array = json_decode($xmlJson,TRUE);

		foreach ($array['Cube']['Cube']['Cube'] as $key => $value) {
			if($letezik->num == 0)
				$this->countuiesModel->setCurrency($value["@attributes"]["currency"],$value["@attributes"]["rate"]);
			else {
				$this->countuiesModel->setCurrencyLogikaiTorles();
				$this->countuiesModel->setCurrency($value["@attributes"]["currency"],$value["@attributes"]["rate"]);
			}
		}
		

		$this->load->view('header');
		$this->load->view('welcome_message');
		$this->load->view('footer');
	}
}